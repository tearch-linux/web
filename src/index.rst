What is TeArch-Linux
====================
TeaArch-Linux is arch linux based end user distribution. TeArch linux provide easy installation and usage for archlinux.

TeArch-linux is Turkiye origin distribution. 🇹🇷 Our main puppose is creating easy usable GNU/Linux for everyone. 

Features
^^^^^^^^

* TeArch-linux uses 17g installer. 17g is (also called ggggggggggggggggg) written Python and Gtk. It is better for gtk based desktop environments.
* TeArch-linux iso profiles are not archiso. We are created teaiso. Teaiso is multi purposhe iso creating tools written python3 and C.
* TeArch-linux rebuild some packages for debloating and better stability.

TeArch-linux support most popular desktop environments.

* Gnome
* Xfce
* Pantheon
* Mate
* Cinnamon

Support & Community
^^^^^^^^^^^^^^^^^^^

You can join our `Telegram <https://t.me/TeArchlinux>`_ community.

You can visit `github <https://github.com/TeArch-linux>`_ or `gitlab <https://gitlab.com/TeArch-linux>`_ page.

Downloads
^^^^^^^^^
Please visit `Github Release <https://github.com/TeArch-linux/releases/releases>`_ page
